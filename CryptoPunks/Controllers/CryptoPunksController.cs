﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CryptoPunks.Domain;
using CryptoPunks.Service;
using Microsoft.AspNetCore.Mvc;

namespace CryptoPunks.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CryptoPunksController : ControllerBase
    {
        private readonly ICryptoPunkService _cryptoPunkService;

        public CryptoPunksController(ICryptoPunkService cryptoPunkService)
        {
            _cryptoPunkService = cryptoPunkService;
        }

        [HttpGet("{id}")]
        public async Task<CryptoPunk> Get(int id)
        {
            return await _cryptoPunkService.GetByIndex(id);
        }

        [HttpGet]
        public async Task<IList<CryptoPunkOffer>> GetOfferedForSale()
        {
            return await _cryptoPunkService.GetOfferedForSale();
        }
    }
}
