﻿using CryptoPunks.Data.Exceptions;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace CryptoPunks.Middleware
{
    public class ErrorHandlingMiddleware
    {
        public async Task Invoke(HttpContext context)
        {
            context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;

            var ex = context.Features.Get<IExceptionHandlerFeature>()?.Error;
            if (ex == null)
            {
                return;
            }
                
            if (ex is PunkNotFoundException)
            {
                context.Response.StatusCode = (int) HttpStatusCode.NotFound;
            }

            var error = new
            {
                message = context.Response.StatusCode == (int)HttpStatusCode.InternalServerError ? "An error happened. Please contact administrator." : ex.Message
            };

            context.Response.ContentType = "application/json";

            using (var writer = new StreamWriter(context.Response.Body))
            {
                new JsonSerializer().Serialize(writer, error);
                await writer.FlushAsync().ConfigureAwait(false);
            }
        }
    }
}
