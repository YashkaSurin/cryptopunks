﻿using CryptoPunks.Data.Market;
using CryptoPunks.Data.Profile;
using CryptoPunks.Hosted;
using CryptoPunks.Middleware;
using CryptoPunks.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace CryptoPunks
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(b => { b.AddConsole(); });
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(options => options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore);

            services.AddSingleton(typeof(ICryptoPunkMarketDataProvider), _ => new Web3CryptoPunkMarketDataProvider(
                Configuration.GetValue<string>("CryptoPunks:EthereumUrl"),
                Configuration.GetValue<string>("CryptoPunks:ContractAddress")
            ));
            services.AddTransient(typeof(ICryptoPunkProfileSourceReader), _ => new HttpCryptoPunkProfileSourceReader(
                Configuration.GetValue<string>("CryptoPunks:ProfilesSourceUrl")
            ));
            services.AddSingleton<ICryptoPunkProfileRepository, JsonCryptoPunkProfileRepository>();
            services.AddSingleton<ICryptoPunkMarketRepository, EthCryptoPunkMarketRepository>();
            services.AddTransient<ICryptoPunkService, CryptoPunkService>();
            services.AddSingleton<IHostedService, MarketDataRefreshHostedService>();
        }

        public async void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            app.UseExceptionHandler(new ExceptionHandlerOptions
            {
                ExceptionHandler = new ErrorHandlingMiddleware().Invoke
            });

            app.UseMvc();

            var logger = app.ApplicationServices.GetService<ILogger<Startup>>();
            using (var scope = app.ApplicationServices.CreateScope())
            {              
                var profileRepo = scope.ServiceProvider.GetService<ICryptoPunkProfileRepository>();
                logger.LogInformation("Initializing punk profiles storage...");
                await profileRepo.Init();
                logger.LogInformation("Punk profiles storage initialized.");
            }

            logger.LogInformation("The application has started.");
        }
    }
}
