﻿using CryptoPunks.Data.Market;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CryptoPunks.Hosted
{
    public class MarketDataRefreshHostedService : HostedService
    {
        private readonly ICryptoPunkMarketRepository _cryptoPunkMarketRepository;
        private readonly ILogger<MarketDataRefreshHostedService> _logger;

        public MarketDataRefreshHostedService(ICryptoPunkMarketRepository cryptoPunkMarketRepository, ILogger<MarketDataRefreshHostedService> logger)
        {
            _cryptoPunkMarketRepository = cryptoPunkMarketRepository;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (true)
            {
                try
                {
                    _logger.LogInformation("Retrieving market data...");
                    await _cryptoPunkMarketRepository.Update();
                    _logger.LogInformation("Market data updated.");
                }
                catch (Exception e)
                {
                    _logger.LogError("Could not update market data. ", e);
                }
                
                await Task.Delay(TimeSpan.FromMinutes(1));
            }
        }
    }
}
