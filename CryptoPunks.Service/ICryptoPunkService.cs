﻿using CryptoPunks.Domain;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;

namespace CryptoPunks.Service
{
    public interface ICryptoPunkService
    {
        Task<CryptoPunk> GetByIndex(BigInteger index);
        Task<IList<CryptoPunkOffer>> GetOfferedForSale();
    }
}
