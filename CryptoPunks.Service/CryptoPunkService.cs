﻿using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;
using CryptoPunks.Data.Market;
using CryptoPunks.Data.Profile;
using CryptoPunks.Domain;

namespace CryptoPunks.Service
{
    public class CryptoPunkService : ICryptoPunkService
    {
        private readonly ICryptoPunkProfileRepository _cryptoPunkProfileRepository;
        private readonly ICryptoPunkMarketRepository _cryptoPunkMarketRepository;

        public CryptoPunkService(ICryptoPunkProfileRepository cryptoPunkProfileRepository, ICryptoPunkMarketRepository cryptoPunkMarketRepository)
        {
            _cryptoPunkProfileRepository = cryptoPunkProfileRepository;
            _cryptoPunkMarketRepository = cryptoPunkMarketRepository;
        }

        public async Task<IList<CryptoPunkOffer>> GetOfferedForSale()
        {
            return await _cryptoPunkMarketRepository.GetOfferedForSale();
        }

        public async Task<CryptoPunk> GetByIndex(BigInteger index)
        {
            var profile = await _cryptoPunkProfileRepository.GetById(index);
            var offer = await _cryptoPunkMarketRepository.GetByPunkId(index);

            return new CryptoPunk
            {
                Id = index,
                Profile = profile,
                Offer = offer
            };
        }
    }
}
