﻿using CryptoPunks.Domain;
using System;
using System.Numerics;
using System.Threading.Tasks;

namespace CryptoPunks.Data.Profile
{
    public interface ICryptoPunkProfileRepository
    {
        Task<CryptoPunkProfile> GetById(BigInteger id);
        Task Init();
    }
}
