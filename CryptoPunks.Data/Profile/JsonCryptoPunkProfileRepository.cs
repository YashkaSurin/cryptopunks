﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;
using CryptoPunks.Data.Exceptions;
using CryptoPunks.Domain;
using Newtonsoft.Json;

namespace CryptoPunks.Data.Profile
{
    public class JsonCryptoPunkProfileRepository : ICryptoPunkProfileRepository
    {
        private Dictionary<BigInteger, CryptoPunkProfile> _profiles;
        private readonly ICryptoPunkProfileSourceReader _sourceReader;

        public JsonCryptoPunkProfileRepository(ICryptoPunkProfileSourceReader sourceReader)
        {
            _sourceReader = sourceReader;
        }

        public async Task Init()
        {
            if (_profiles != null)
            {
                return;
            }

            try
            {
                var json = await _sourceReader.ReadAll();
                _profiles = JsonConvert.DeserializeObject<Dictionary<BigInteger, CryptoPunkProfile>>(json);
            }
            catch (Exception e)
            {
                throw new DataSourceInitializationException($"Could not read or parse profile data.", e);
            }
        }


        public Task<CryptoPunkProfile> GetById(BigInteger id)
        {
            if (_profiles == null)
            {
                throw new DataSourceInitializationException("The crypto punks profile data has not been initialized.");
            }

            if (_profiles.TryGetValue(id, out var profile))
            {
                return Task.FromResult(profile);
            }
            else
            {
                throw new PunkNotFoundException(id);
            }
        }
    }
}
