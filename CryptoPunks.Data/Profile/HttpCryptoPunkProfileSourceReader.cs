﻿using System.Net.Http;
using System.Threading.Tasks;

namespace CryptoPunks.Data.Profile
{
    public class HttpCryptoPunkProfileSourceReader : ICryptoPunkProfileSourceReader
    {
        private readonly string _profileSourceUrl;

        public HttpCryptoPunkProfileSourceReader(string url)
        {
            _profileSourceUrl = url;
        }

        public async Task<string> ReadAll()
        {
            using (var client = new HttpClient())
            {
                using (var result = await client.GetAsync(_profileSourceUrl))
                {
                    if (result.IsSuccessStatusCode)
                    {
                        return await result.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        throw new HttpRequestException($"Could not load file from url: {_profileSourceUrl}. Error: {result.StatusCode}");
                    }
                }
            }
        }
    }
}
