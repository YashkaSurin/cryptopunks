﻿using System.Threading.Tasks;

namespace CryptoPunks.Data.Profile
{
    public interface ICryptoPunkProfileSourceReader
    {
        Task<string> ReadAll();
    }
}
