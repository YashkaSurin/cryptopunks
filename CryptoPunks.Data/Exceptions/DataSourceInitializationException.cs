﻿using System;

namespace CryptoPunks.Data.Exceptions
{
    public class DataSourceInitializationException: Exception
    {
        public DataSourceInitializationException(string message, Exception innerException = null): base(message, innerException)
        {
        }
    }
}
