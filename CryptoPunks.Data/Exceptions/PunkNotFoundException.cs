﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace CryptoPunks.Data.Exceptions
{
    public class PunkNotFoundException: Exception
    {
        private readonly BigInteger _id;
        
        public PunkNotFoundException(BigInteger id)
        {
            _id = id;
        }

        public override string Message => "Punk not found: " + _id;
    }
}
