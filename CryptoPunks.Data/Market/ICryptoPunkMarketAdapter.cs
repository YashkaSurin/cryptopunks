﻿using CryptoPunks.Domain.CryptoPunksMarket.ContractDefinition;
using Nethereum.Contracts;
using Nethereum.Hex.HexTypes;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;

namespace CryptoPunks.Data.Market
{
    public interface ICryptoPunkMarketDataProvider
    {
        Task<PunksOfferedForSaleOutputDTO> GetPunkOfferedForSale(BigInteger id);
        Task<List<EventLog<PunkOfferedEventDTO>>> GetPunkOfferedUpdates(HexBigInteger fromBlock);
        Task<List<EventLog<PunkNoLongerForSaleEventDTO>>> GetPunkNoLongerForSaleUpdates(HexBigInteger fromBlock);
    }
}
