﻿using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;
using CryptoPunks.Domain.CryptoPunksMarket;
using CryptoPunks.Domain.CryptoPunksMarket.ContractDefinition;
using Nethereum.Contracts;
using Nethereum.Hex.HexTypes;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Web3;

namespace CryptoPunks.Data.Market
{
    public class Web3CryptoPunkMarketDataProvider : ICryptoPunkMarketDataProvider
    {
        private readonly CryptoPunksMarketService _cryptoPunksMarketService;
        private readonly Web3 _web3;

        private readonly Event<PunkOfferedEventDTO> _punkOfferedEvent;
        private readonly Event<PunkNoLongerForSaleEventDTO> _punkNoLongerForSaleEvent;

        public Web3CryptoPunkMarketDataProvider(string url, string contractAddress)
        {
            _web3 = new Web3(url);
            _cryptoPunksMarketService = new CryptoPunksMarketService(_web3, contractAddress);

            _punkOfferedEvent = _web3.Eth.GetEvent<PunkOfferedEventDTO>(contractAddress);
            _punkNoLongerForSaleEvent = _web3.Eth.GetEvent<PunkNoLongerForSaleEventDTO>(contractAddress);
        }

        public async Task<PunksOfferedForSaleOutputDTO> GetPunkOfferedForSale(BigInteger id)
        {
            return await _cryptoPunksMarketService.PunksOfferedForSaleQueryAsync(id);
        }

        public async Task<List<EventLog<PunkOfferedEventDTO>>> GetPunkOfferedUpdates(HexBigInteger fromBlock)
        {
            var punkOfferedEventFilter = _punkOfferedEvent.CreateFilterInput();
            punkOfferedEventFilter.FromBlock = new BlockParameter(fromBlock);
            return await _punkOfferedEvent.GetAllChanges(punkOfferedEventFilter);
        }

        public async Task<List<EventLog<PunkNoLongerForSaleEventDTO>>> GetPunkNoLongerForSaleUpdates(HexBigInteger fromBlock)
        {
            var punkNoLongerForSaleEventFilter = _punkNoLongerForSaleEvent.CreateFilterInput();
            punkNoLongerForSaleEventFilter.FromBlock = new BlockParameter(fromBlock);
            return await _punkNoLongerForSaleEvent.GetAllChanges(punkNoLongerForSaleEventFilter);
        }
    }
}
