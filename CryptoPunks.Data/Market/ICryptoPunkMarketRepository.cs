﻿using CryptoPunks.Domain;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;

namespace CryptoPunks.Data.Market
{
    public interface ICryptoPunkMarketRepository
    {
        Task<CryptoPunkOffer> GetByPunkId(BigInteger id);
        Task Update();
        Task<IList<CryptoPunkOffer>> GetOfferedForSale();
    }
}
