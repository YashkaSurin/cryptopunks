﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using CryptoPunks.Domain;
using Nethereum.Hex.HexTypes;

namespace CryptoPunks.Data.Market
{
    public class EthCryptoPunkMarketRepository : ICryptoPunkMarketRepository
    {
        private readonly ICryptoPunkMarketDataProvider _cryptoPunksMarketAdapter;
        private ConcurrentDictionary<BigInteger, CryptoPunkOffer> _offeredForSale = new ConcurrentDictionary<BigInteger, CryptoPunkOffer>();
        private HexBigInteger _punkOfferedLastBlock = new HexBigInteger(0);
        private HexBigInteger _punkNoLongerForSaleLastBlock = new HexBigInteger(0);

        public EthCryptoPunkMarketRepository(ICryptoPunkMarketDataProvider adapter)
        {
            _cryptoPunksMarketAdapter = adapter;
        }

        public Task<IList<CryptoPunkOffer>> GetOfferedForSale()
        {
            var result = new List<CryptoPunkOffer>(_offeredForSale.Values);
            return Task.FromResult<IList<CryptoPunkOffer>>(result);
        }

        public async Task<CryptoPunkOffer> GetByPunkId(BigInteger id)
        {
            // NOTE we don't have the seller information in our cache (as the PunkOffered event does not have it),
            // so we have to get this info from Ethereum. The more performant way would be consuming other types of events too
            // to track the punk owner, however I think for this test assignment the current approach is fine.

            var punksOfferedForSaleDto = await _cryptoPunksMarketAdapter.GetPunkOfferedForSale(id);
            var isForSale = punksOfferedForSaleDto.IsForSale;
            return new CryptoPunkOffer
            {
                IsForSale = isForSale,
                MinValue = isForSale ? (BigInteger?)punksOfferedForSaleDto.MinValue : null,
                OnlySellTo = isForSale ? punksOfferedForSaleDto.OnlySellTo : null,
                PunkIndex = isForSale ? (BigInteger?)punksOfferedForSaleDto.PunkIndex : null,
                Seller = isForSale ? punksOfferedForSaleDto.Seller : null
            };
        }

        public async Task Update()
        {
            // get all "punk offered" and "punk no longer for sale" events since the last update

            var punkOfferedEvents = await _cryptoPunksMarketAdapter.GetPunkOfferedUpdates(_punkOfferedLastBlock);
            var punkNoLongerForSaleEvents = await _cryptoPunksMarketAdapter.GetPunkNoLongerForSaleUpdates(_punkNoLongerForSaleLastBlock);
            var offersWithBlockNumbers = new List<Tuple<string, BigInteger, CryptoPunkOffer>>();

            offersWithBlockNumbers.AddRange(punkOfferedEvents.Select(e => new Tuple<string, BigInteger, CryptoPunkOffer> (
                e.Log.BlockNumber.ToString(),
                e.Event.PunkIndex,
                new CryptoPunkOffer
                {
                    IsForSale = true,
                    MinValue = e.Event.MinValue,
                    OnlySellTo = e.Event.ToAddress,
                    PunkIndex = e.Event.PunkIndex
                }
            )));

            offersWithBlockNumbers.AddRange(punkNoLongerForSaleEvents.Select(e => new Tuple<string, BigInteger, CryptoPunkOffer> (
                e.Log.BlockNumber.ToString(),
                e.Event.PunkIndex,
                new CryptoPunkOffer { IsForSale = false }
            )));

            // sort them by block number

            offersWithBlockNumbers.Sort(new EventBlockNumberComparer());

            // write to the map in correct order

            foreach (var offerWithBlockNumber in offersWithBlockNumbers)
            {
                var offer = offerWithBlockNumber.Item3;
                if (offer.IsForSale)
                {
                    _offeredForSale[offerWithBlockNumber.Item2] = offer;
                }
                else
                {
                    if (_offeredForSale.ContainsKey(offerWithBlockNumber.Item2))
                    {
                        _offeredForSale.Remove(offerWithBlockNumber.Item2, out var _);
                    }
                }
            }

            // update the last block pointers

            if (punkOfferedEvents.Count > 0)
            {
                _punkOfferedLastBlock = punkOfferedEvents[punkOfferedEvents.Count - 1].Log.BlockNumber;
            }

            if (punkNoLongerForSaleEvents.Count > 0)
            {
                _punkNoLongerForSaleLastBlock = punkNoLongerForSaleEvents[punkNoLongerForSaleEvents.Count - 1].Log.BlockNumber;
            }
        }

        private class EventBlockNumberComparer : IComparer<Tuple<string, BigInteger, CryptoPunkOffer>>
        {
            public int Compare(Tuple<string, BigInteger, CryptoPunkOffer> x, Tuple<string, BigInteger, CryptoPunkOffer> y)
            {
                return x.Item1.CompareTo(y.Item1);
            }
        }
    }
}
