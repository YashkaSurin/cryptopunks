# Running the application

There are two options:

a) Build a Docker image from the Dockerfile and run it:

`$ docker build -t cryptopunks .`  
`$ docker run -p 5000:80 cryptopunks`

b) Run on your host machine (.NET Core 2.1 or higher is required):

`$ cd CryptoPunks/`  
`$ dotnet run`

The application will listen to the port 5000.

# API

## Get punk by id

`$ curl http://localhost:5000/api/cryptopunks/510`

Sample responses:

a) Punk exists and is offered for sale

```
200 OK
{"id":510,"profile":{"gender":"Male","accessories":[]},"offer":{"isForSale":true,"punkIndex":510,"seller":"0xe8723d26ad5f7ff4ea8902d235f0a8f8b8802f6d","minValue":13750000000000000000,"onlySellTo":"0x0000000000000000000000000000000000000000"}}
```

b) Punk exists and is NOT offered for sale

```
200 OK
{"id":1,"profile":{"gender":"Male","accessories":["Smile","Mohawk"]},"offer":{"isForSale":false}}
```

c) Punk does not exist

```
404 Not Found
{"message":"Punk not found: 100500"}
```

## Get all punks that are offered for sale

`$ curl http://localhost:5000/api/cryptopunks`

Sample response:

```
200 OK
[{"isForSale":true,"punkIndex":0,"minValue":100000000000000000000,"onlySellTo":"0x0000000000000000000000000000000000000000"},{"isForSale":true,"punkIndex":8960,"minValue":1000000000000000000,"onlySellTo":"0x0000000000000000000000000000000000000000"}
```

Please note that initialization of the market data takes some time on startup (less than 10 seconds on my machine).

# Some technical information

The application is written in .NET Core. Libraries used:  
- ASP.NET Core for the web infrastructure,  
- xUnit and Moq for testing,  
- Nethereum for communication with the Ethereum network.
