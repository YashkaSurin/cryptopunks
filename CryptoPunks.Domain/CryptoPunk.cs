﻿using System.Numerics;

namespace CryptoPunks.Domain
{
    public class CryptoPunk
    {
        public BigInteger Id { get; set; }
        public CryptoPunkProfile Profile { get; set; }
        public CryptoPunkOffer Offer { get; set; }
    }
}
