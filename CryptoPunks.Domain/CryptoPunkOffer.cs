﻿using System.Numerics;

namespace CryptoPunks.Domain
{
    public class CryptoPunkOffer
    {
        public bool IsForSale { get; set; }
        public BigInteger? PunkIndex { get; set; }
        public string Seller { get; set; }
        public BigInteger? MinValue { get; set; }
        public string OnlySellTo { get; set; }
    }
}
