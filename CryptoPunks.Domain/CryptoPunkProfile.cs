﻿using System;

namespace CryptoPunks.Domain
{
    public class CryptoPunkProfile
    {
        public string Gender { get; set; }
        public string[] Accessories { get; set; }
    }
}
