﻿using CryptoPunks.Data.Exceptions;
using CryptoPunks.Data.Profile;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using Xunit;

namespace CryptoPunks.Test.Data
{
    public class JsonCryptoPunkProfileRepositoryTest
    {
        [Fact]
        public async void ShouldInitFromValidSource()
        {
            var repository = new JsonCryptoPunkProfileRepository(MockValidReader());
            await repository.Init();

            Assert.NotNull(repository.GetById(1));
        }

        [Fact]
        public async void ShouldThrowCorrectExceptionWhenCouldNotDownloadFromSource()
        {
            var innerEx = new InvalidOperationException();
            var sourceReaderMock = new Mock<ICryptoPunkProfileSourceReader>();
            sourceReaderMock.Setup(r => r.ReadAll()).ThrowsAsync(innerEx);

            var repository = new JsonCryptoPunkProfileRepository(sourceReaderMock.Object);
            var ex = await Assert.ThrowsAsync<DataSourceInitializationException>(repository.Init);

            Assert.Equal("Could not read or parse profile data.", ex.Message);
            Assert.Equal(innerEx, ex.InnerException);
        }

        [Fact]
        public async void ShouldThrowCorrectExceptionWhenCouldNotParseSource()
        {
            var sourceReaderMock = new Mock<ICryptoPunkProfileSourceReader>();
            sourceReaderMock.Setup(r => r.ReadAll()).ReturnsAsync(@"invalid json");

            var repository = new JsonCryptoPunkProfileRepository(sourceReaderMock.Object);
            var ex = await Assert.ThrowsAsync<DataSourceInitializationException>(repository.Init);

            Assert.Equal("Could not read or parse profile data.", ex.Message);
            Assert.NotNull(ex.InnerException);
        }

        [Fact]
        public async void ShouldGetExistingProfile()
        {
            var repository = new JsonCryptoPunkProfileRepository(MockValidReader());
            await repository.Init();

            var profile = await repository.GetById(2);
            Assert.Equal("Female", profile.Gender);
            Assert.Equal(new[] { "Wild Hair", "Pipe" }, profile.Accessories);
        }

        [Fact]
        public async void ShouldThrowCorrectExceptionOnGetNonExistingProfile()
        {
            var repository = new JsonCryptoPunkProfileRepository(MockValidReader());
            await repository.Init();
            var ex = await Assert.ThrowsAsync<PunkNotFoundException>(() => repository.GetById(3));

            Assert.Equal("Punk not found: 3", ex.Message);
        }

        [Fact]
        public async void ShouldThrowCorrectExceptionOnGetWithoutInit()
        {
            var repository = new JsonCryptoPunkProfileRepository(MockValidReader());
            var ex = await Assert.ThrowsAsync<DataSourceInitializationException>(() => repository.GetById(3));

            Assert.Equal("The crypto punks profile data has not been initialized.", ex.Message);
        }

        private static ICryptoPunkProfileSourceReader MockValidReader()
        {
            var sourceReaderMock = new Mock<ICryptoPunkProfileSourceReader>();
            sourceReaderMock.Setup(r => r.ReadAll()).ReturnsAsync(@"{""1"":{""gender"":""Male"",""accessories"":[""Smile"",""Mohawk""]},""2"":{""gender"":""Female"",""accessories"":[""Wild Hair"",""Pipe""]}}");
            return sourceReaderMock.Object;
        }

    }
}
