﻿using CryptoPunks.Data.Market;
using CryptoPunks.Domain.CryptoPunksMarket.ContractDefinition;
using Moq;
using Nethereum.Contracts;
using Nethereum.Hex.HexTypes;
using Nethereum.RPC.Eth.DTOs;
using System.Collections.Generic;
using System.Numerics;
using Xunit;

namespace CryptoPunks.Test.Data
{
    public class EthCryptoPunksMarketRepositoryTest
    {
        private readonly EthCryptoPunkMarketRepository _repository;
        private readonly Mock<ICryptoPunkMarketDataProvider> _adapterMock;

        public EthCryptoPunksMarketRepositoryTest()
        {
            _adapterMock = MockMarketAdapter();
            _repository = new EthCryptoPunkMarketRepository(_adapterMock.Object);
            _repository.Update().Wait();
        }

        [Fact]
        public async void ShouldGetOfferForPunkOnSale()
        {
            var offer = await _repository.GetByPunkId(2);

            Assert.True(offer.IsForSale);
            Assert.Equal(1, offer.MinValue);
            Assert.Equal("0x1234", offer.OnlySellTo);
            Assert.Equal(2, offer.PunkIndex);
            Assert.Equal("0x5678", offer.Seller);
        }

        [Fact]
        public async void ShouldGetOfferForPunkNotOnSale()
        {
            var offer = await _repository.GetByPunkId(1);

            Assert.False(offer.IsForSale);
            Assert.Null(offer.MinValue);
            Assert.Null(offer.OnlySellTo);
            Assert.Null(offer.PunkIndex);
            Assert.Null(offer.Seller);
        }

        [Fact]
        public async void ShouldGetAllOfferedForSale()
        {
            var offeredForSale = await _repository.GetOfferedForSale();

            Assert.Equal(1, offeredForSale.Count);
            Assert.Equal(2, offeredForSale[0].PunkIndex);
        }

        [Fact]
        public async void ShouldApplyUpdates()
        {
            var addedPunkDto = new PunkOfferedEventDTO { PunkIndex = 1 };
            var addedPunkLog = new FilterLog { BlockNumber = new HexBigInteger(4)};

            var removedPunkDto = new PunkNoLongerForSaleEventDTO { PunkIndex = 2 };
            var removedPunkLog = new FilterLog { BlockNumber = new HexBigInteger(4) };

            _adapterMock.Setup(a => a.GetPunkOfferedUpdates(It.IsAny<HexBigInteger>())).ReturnsAsync(new List<EventLog<PunkOfferedEventDTO>>
                {
                    new EventLog<PunkOfferedEventDTO>(addedPunkDto, addedPunkLog),
                }
            );

            _adapterMock.Setup(a => a.GetPunkNoLongerForSaleUpdates(It.IsAny<HexBigInteger>())).ReturnsAsync(new List<EventLog<PunkNoLongerForSaleEventDTO>>
                {
                    new EventLog<PunkNoLongerForSaleEventDTO>(removedPunkDto, removedPunkLog),
                }
            );

            await _repository.Update();

            var offeredForSale = await _repository.GetOfferedForSale();
            Assert.Equal(1, offeredForSale.Count);
            Assert.Equal(1, offeredForSale[0].PunkIndex);
        }

        private static Mock<ICryptoPunkMarketDataProvider> MockMarketAdapter()
        {
            var addedPunk1EventDto = new PunkOfferedEventDTO
            {
                MinValue = 1,
                PunkIndex = 1,
                ToAddress = "0x1234"
            };

            var addedPunk1FilterLog = new FilterLog
            {
                BlockNumber = new HexBigInteger(1),
                Address = "0x5678"
            };

            var removedPunk1EventDto = new PunkNoLongerForSaleEventDTO
            {
                PunkIndex = 1
            };

            var removedPunk1FilterLog = new FilterLog
            {
                BlockNumber = new HexBigInteger(2)
            };

            var addedPunk2EventDto = new PunkOfferedEventDTO
            {
                MinValue = 1,
                PunkIndex = 2,
                ToAddress = "0x1234"
            };

            var addedPunk2FilterLog = new FilterLog
            {
                BlockNumber = new HexBigInteger(3)
            };

            var addedPunk2Dto = new PunksOfferedForSaleOutputDTO
            {
                IsForSale = true,
                MinValue = addedPunk2EventDto.MinValue,
                OnlySellTo = addedPunk2EventDto.ToAddress,
                PunkIndex = addedPunk2EventDto.PunkIndex,
                Seller = "0x5678"
            };

            var adapterMock = new Mock<ICryptoPunkMarketDataProvider>();
            adapterMock.Setup(a => a.GetPunkOfferedForSale(It.IsAny<BigInteger>())).ReturnsAsync(new PunksOfferedForSaleOutputDTO { IsForSale = false });
            adapterMock.Setup(a => a.GetPunkOfferedForSale(2)).ReturnsAsync(addedPunk2Dto);
            adapterMock.Setup(a => a.GetPunkOfferedUpdates(It.IsAny<HexBigInteger>())).ReturnsAsync(new List<EventLog<PunkOfferedEventDTO>>
                {
                    new EventLog<PunkOfferedEventDTO>(addedPunk1EventDto, addedPunk1FilterLog),
                    new EventLog<PunkOfferedEventDTO>(addedPunk2EventDto, addedPunk2FilterLog)
                }
            );

            adapterMock.Setup(a => a.GetPunkNoLongerForSaleUpdates(It.IsAny<HexBigInteger>())).ReturnsAsync(new List<EventLog<PunkNoLongerForSaleEventDTO>>
                {
                    new EventLog<PunkNoLongerForSaleEventDTO>(removedPunk1EventDto, removedPunk1FilterLog)
                }
            );

            return adapterMock;
        }
    }
}
