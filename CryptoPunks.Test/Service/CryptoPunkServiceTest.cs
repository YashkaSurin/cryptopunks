﻿using CryptoPunks.Data.Exceptions;
using CryptoPunks.Data.Market;
using CryptoPunks.Data.Profile;
using CryptoPunks.Domain;
using CryptoPunks.Service;
using Moq;
using System.Collections.Generic;
using System.Numerics;
using Xunit;

namespace CryptoPunks.Test.Service
{
    public class CryptoPunkServiceTest
    {
        private static readonly CryptoPunkProfile _profile = new CryptoPunkProfile
        {
            Gender = "Female",
            Accessories = new[] { "Hat", "Cat" }
        };

        private static readonly CryptoPunkOffer _offer = new CryptoPunkOffer
        {
            IsForSale = true,
            MinValue = 1,
            OnlySellTo = "0x1234",
            PunkIndex = 1,
            Seller = "0x5678"
        };

        private readonly CryptoPunkService _service;

        public CryptoPunkServiceTest()
        {
            _service = new CryptoPunkService(MockProfileRepo(), MockMarketRepo());
        }

        [Fact]
        public async void ShouldReturnPunk()
        {
            var punk = await _service.GetByIndex(1);

            Assert.Equal(1, punk.Id);
            Assert.Equal(_profile, punk.Profile);
            Assert.Equal(_offer, punk.Offer);
        }

        [Fact]
        public async void ShouldThrowCorrectExceptionWhenPunkIsNotFound()
        {
            await Assert.ThrowsAsync<PunkNotFoundException>(() => _service.GetByIndex(2));
        }

        [Fact]
        public async void ShouldReturnOfferedForSale()
        {
            var offeredForSale = await _service.GetOfferedForSale();

            Assert.Equal(1, offeredForSale.Count);
            Assert.Equal(1, offeredForSale[0].PunkIndex);
        }

        private static ICryptoPunkProfileRepository MockProfileRepo()
        {
            var profileRepoMock = new Mock<ICryptoPunkProfileRepository>();
            profileRepoMock.Setup(r => r.GetById(1)).ReturnsAsync(_profile);
            profileRepoMock.Setup(r => r.GetById(2)).ThrowsAsync(new PunkNotFoundException(2));

            return profileRepoMock.Object;
        }

        private static ICryptoPunkMarketRepository MockMarketRepo()
        {
            var marketRepoMock = new Mock<ICryptoPunkMarketRepository>();
            marketRepoMock.Setup(r => r.GetByPunkId(1)).ReturnsAsync(_offer);
            marketRepoMock.Setup(r => r.GetOfferedForSale()).ReturnsAsync(new[] { _offer });

            return marketRepoMock.Object;
        }
    }
}
