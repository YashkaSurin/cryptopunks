FROM mcr.microsoft.com/dotnet/core/aspnet:2.1-stretch-slim AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:2.1-stretch AS build
WORKDIR /src
COPY ["CryptoPunks/CryptoPunks.csproj", "CryptoPunks/"]
RUN dotnet restore "CryptoPunks/CryptoPunks.csproj"
COPY . .
WORKDIR "/src/CryptoPunks"
RUN dotnet build "CryptoPunks.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "CryptoPunks.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "CryptoPunks.dll"]